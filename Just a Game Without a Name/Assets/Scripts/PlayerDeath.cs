﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    public GameObject player;
    public Transform spawnLocation;
    public void OnTriggerEnter(Collider other)
    {
        player.gameObject.transform.position = spawnLocation.transform.position;
        player.gameObject.transform.rotation = spawnLocation.transform.rotation;

    }
}
