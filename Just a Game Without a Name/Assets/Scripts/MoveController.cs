using UnityEngine;
public class MoveController : MonoBehaviour
{
    public float speed;
    public float jumpStrength;
    public float turnSpeed;
    private Animator anim;

    Rigidbody rb;
    CapsuleCollider col;

    void Start()
    {

        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>(); 
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(h, 0, v);
        transform.Rotate(0, h * turnSpeed, 0);

        if (isGrounded() && Input.GetButton("Jump"))
        {
            rb.AddForce(Vector3.up * jumpStrength, ForceMode.Impulse);
            anim.SetInteger("AnimationPar", 2);
        }
        if (isGrounded() && Input.GetKey("w") || Input.GetKey("s"))
        {
                anim.SetInteger("AnimationPar", 1);
        }
        else if (isGrounded())
        {
            anim.SetInteger("AnimationPar", 0);
        }
        else
        {
            anim.SetInteger("AnimationPar", 3);
        }
    }

    bool isGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, col.bounds.extents.y + 1f);
    }
}
 