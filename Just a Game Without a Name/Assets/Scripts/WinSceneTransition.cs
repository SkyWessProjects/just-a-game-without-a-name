﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class WinSceneTransition : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {

        SceneManager.LoadScene("WinScreen");
    }
}
