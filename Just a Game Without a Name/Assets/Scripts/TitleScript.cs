﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScript : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("SceneOne3DPlatformer");
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
