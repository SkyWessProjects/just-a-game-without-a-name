﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Collectables : MonoBehaviour
{
    private int count;
    public TextMeshProUGUI countText;
    
    void Start()
    {
        count = 0;
        SetCountText();
    }
 
    void SetCountText()
    {
        countText.text = "Rovers: " + count.ToString() + "/4";
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }
}
