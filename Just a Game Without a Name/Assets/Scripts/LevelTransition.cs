﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition : MonoBehaviour
{
    private Animator anime;
    public GameObject player;
    public GameObject obj;
    public Transform spawnLocation;
    void OnTriggerEnter(Collider other)
    {
        player.gameObject.transform.position = spawnLocation.transform.position;
        player.gameObject.transform.rotation = spawnLocation.transform.rotation;
        Destroy(player.GetComponent<Rigidbody>());
        anime = obj.GetComponent<Animator>();
        StartCoroutine(Anime());
    }
    public IEnumerator Anime()
    {
        anime.enabled = !anime.enabled;
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("SceneTwo");
    }
}
